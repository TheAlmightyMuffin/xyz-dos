import os, sys, json

def adventure2game():
    clear()
    load()
    print "Adventure 2"
    print "\n You arive at an abandoned hotel."
    print "What do you do?"
    print "\nGo >in."
    print "I'M >OUTTA HERE!"
    dinput()
    if temp == "in":
        print "You walk in."
        print "You feel a cold breeze crawl down your spine"
        print "\nGo through >left door."
        print "Go through >right door."
        dinput()
        if temp == "left":
            print "You fall, as there was no floor."
        if temp == "right":
            print "You enter the room. There is a man-eating bear in the room."
            print "\n>Attack the bear"
            print ">RUN"
            dinput()
            if temp == "attack":
                print "It wasn't as easy as the monster... You died"
            if temp == "run" or "RUN":
                print "You got out of there, luring the bear out of the hotel."
                print "In the end, you never got ate."
                print "Earned 10 X-Points."
                allvar['points'] += 10
    if temp == "OUTTA" or "outta":
        print "You walk away."
    else:
        print "die"
    save()

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()