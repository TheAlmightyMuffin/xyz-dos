import os, sys, json

def xedit():
    clear()
    print """---xEDIT------------------------------------
\nNOTICE: This editor is buggy. Consider using one of the linux editors listing, if you're on linux.
\nChoose an operation
\n>New file
>View file
>nano
>vim
>Exit"""
    dinput()
    if temp == "new":
        clear()
        print "File name?"
        filename = raw_input()
        clear()
        print "---xEDIT-----------------------------------"
        text = raw_input()
        print "---END OF FILE--- S - Save  E - Exit w/o saving"
        dinput()
        if temp == "S" or "s":
            f = open("home/documents/"+filename,'w')
            f.write(text)
            f.close()
            print "Saved!"
    elif temp == "view":
        clear()
        print "File name?"
        dinput()
        clear()
        with open(temp, 'r') as content_file:
            content = content_file.read()
        print content
    elif temp == "nano":
        os.system('nano')
    elif temp == "vim":
        os.system('vim')

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()