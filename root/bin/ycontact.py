import os, sys, time, json

def xcontact():
    print """---yCONTACT------------------------------------
\n Contacts:
>TheMasterX
>unseenhacker
>Rainbow2028
>Find User"""
    dinput()
    if temp == "themasterx":
        contactmasterx()
    else:
        print "Not implemented, or die if you didn't type one of the choices."
def contactmasterx():
    clear()
    if allvar['contactnumberx'] == 0:
        print """---yCONTACT------------------------------------
Calling User TheMasterX..."""
        time.sleep(10)
        print "User Busy."
    if allvar['contactnumberx'] == 1:
        print """---yCONTACT------------------------------------
Calling User TheMasterX..."""
        time.sleep(6)
        print "TheMasterX: Well, I see that the contact package is working."
        time.sleep(4)
        print "TheMasterX: I've been working on a little upgrade to the kernel"
        time.sleep(3.2)
        print "TheMasterX: The yNet."
        time.sleep(5)
        print "TheMasterX: The yNet is what will make the Y in XYZ."
        time.sleep(4)
        print "TheMasterX: You can connect to all the regular websites"
        time.sleep(5)
        print "TheMasterX: And also connect to special yNet websites."
        time.sleep(7)
        print "TheMasterX: Well, I'll leave you to keep upgrading."
        time.sleep(3)
        print "TheMasterX: cyaz"
        time.sleep(2)
        print "User TheMasterX disconnected."
        time.sleep(10)
        allvar['contactnumberx'] = 0
    if allvar['contactnumberx'] == 2:
        print """---yCONTACT------------------------------------
Calling User TheMasterX..."""
        time.sleep(10)
        print "TheMasterX: Welcome back."
        time.sleep(4)
        print "TheMasterX: We have another beta tester that has bought ycontact"
        time.sleep(6)
        print "*TheMasterX is inviting unseenhacker to the call..."
        time.sleep(10)
        print "unseenhacker: Hello."
        time.sleep(6)
        print "unseenhacker: This is quite the interesting project you've got here."
        time.sleep(7)
        print "TheMasterX: Welcome. I have another beta tester here. I don't think Rainbow2028 bought ycontact yet."
        time.sleep(5)
        print "TheMasterX: This testers name is %s" % allvar['name']
        time.sleep(4)
        print "unseenhacker: Hi"
        time.sleep(6)
        print "TheMasterX: Well I have to go."
        time.sleep(4)
        print "TheMasterX: cyaz"
        time.sleep(3)
        print "TheMasterX has went offline..."
        time.sleep(5)
def contactunseenhacker():
    clear()
    if allvar['contactnumberu'] == 0:
        print """---yCONTACT------------------------------------
Calling User unseenhacker..."""
        time.sleep(10)
        print "User Busy."
    elif allvar['contactnumberu'] == 1:
        print """---yCONTACT------------------------------------
Calling User unseenhacker..."""
        time.sleep(10)
        print "unseenhacker: Oh, you got the message."
        time.sleep(3)
        print "unseenhacker: I've been using a custom package to spy on TheMasterX"
    else:
        print "die"
def contactnumberr():
    clear()
    if allvar['contactnumberr'] == 0:
        print """---yCONTACT------------------------------------
Calling User Rainbow2028..."""
        time.sleep(10)
        print "User Busy."
    elif allvar['contactnumberr'] == 1:
        print """---yCONTACT------------------------------------
Calling User Rainbow2028..."""
        time.sleep(10)
        print "Rainbow2028: Oh, hi."
        time.sleep(3)
        print "Rainbow2028: So you got the PM?"
        time.sleep(6)
        print "Rainbow2028: Great!"
        time.sleep(4)
        print "Rainbow2028: Your name is Jenny? Huh, that sounds very familiar."
        time.sleep(5)
        print "Rainbow2028: Oh well. I hear my mom calling for me. Got to go."
        time.sleep(4)
        print "Rainbow2028: See ya!"
        time.sleep(7)
        print "Rainbow2028 has went offline..."
    

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()