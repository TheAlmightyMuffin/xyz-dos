import os, sys, json, time

def hunter():
    load()
    print "Getting repository list..."
    time.sleep(1.2)
    clear()
    print "P-Points:", allvar['hpoints']
    print "\nAvailable Packages:"
    print "unseensh - The Unseen Shell. Required for homebrew packages - 10 P-Points"
    if allvar['unseensh'] == 1:
        print "privatetalk - Private P2P yContact Clone. - 30 P-Points"
        print "x2ppoints - X-Point to P-Point converter. - 100 P-Points"
        print "yspy - Spy on yContact calls. - 500 P-Points"
    cinput("Type what you want >> ")
    
    if temp == "unseensh":
        if allvar['unseensh'] == 1:
            print "You already bought this"
        elif allvar['hpoints'] < 10:
            print "You don't have enough P-Points"
        elif allvar['unseensh'] == 0:
            allvar['unseensh'] = 1
            allvar['hpoints'] -= 10
            print "You bought unseensh!"
    if temp == "privatetalk":
        if allvar['privatetalk'] == 1:
            print "You already bought this"
        elif allvar['hpoints'] < 30:
            print "You don't have enough P-Points"
        elif allvar['privatetalk'] == 0:
            allvar['privatetalk'] = 1
            allvar['pm'] = 1
            allvar['newpm'] += 1
            allvar['hpoints'] -= 30
            print "You bought privatetalk!"
    if temp == "x2ppoints":
        if allvar['x2ppoints'] == 1:
            print "You already bought this"
        elif allvar['hpoints'] < 100:
            print "You don't have enough P-Points"
        elif allvar['x2ppoints'] == 0:
            allvar['x2ppoints'] = 1
            allvar['hpoints'] -= 100
            print "You bought x2ppoints!"
    if temp == "yspy":
        if allvar['yspy'] == 1:
            print "You already bought this"
        elif allvar['hpoints'] < 500:
            print "You don't have enough P-Points"
        elif allvar['yspy'] == 0:
            allvar['yspy'] = 1
            allvar['hpoints'] -= 500
            print "You bought yspy!"
    save()

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()
