import os, sys, json

def yglobe():
    load()
    clear()
    print """---yGLOBE---------------------------------------------------
\nChoose an operation
\n>Recommended Websites
>TheMasterX's Website
>URL
>Exit"""
    dinput()
    if temp == "recommended": yglobe_recommended()
    elif temp == "themasterx": yglobe_masterx()
    elif temp == "url": yglobe_url()
    else: pass

def yglobe_url():
    clear()
    print "---yGLOBE---------------------------------------------------"
    cinput("URL? >>")
    if temp == "themasterx.info" or "www.themasterx.info": yglobe_masterx()
    elif temp == "skyycommunity.xyzforums.org": yglobe_skyycommunity()
    elif temp == "ydownloadhost.com" or "www.ydownloadhost.com": yglobe_ydownloadhost()
    else: pass
    
def yglobe_recommended():
    clear()
    print """---yGLOBE---------------------------------------------------
\n>SkyyCommunity
(DEAD LINK) XYZ-Forums
>YDownloadHost
>Back"""
    dinput()
    if temp == "skyycommunity": yglobe_skyycommunity()
    if temp == "ydownloadhost": yglobe_ydownloadhost()
    
def yglobe_masterx():
    clear()
    print """---yGLOBE-----TheMasterX's Website--------------------------
Enter 'quit' to exit yGlobe

       Welcome to TheMasterX's Website.
------------------------------------------------
>Essays             >XYZ-DOS              >About

News
------------------------------------------------

Why proprietary software was the past, and now the future.
----------------------------------------------------------
2/24/2042

Proprietary software was tooken over by open source. I
have reasons why closed source software should rule again.
	READ >MORE


2042 - TheMasterX
"""
    dinput()
    if temp == "essays": yglobe_masterx_essays()
    elif temp == "xyz-dos" or "xyzdos" or "xyz": yglobe_masterx_xyzdos()
    elif temp == "about": yglobe_masterx_about()
    elif temp == "more": yglobe_masterx_blogpost()
    elif temp == "quit": yglobe()
    else: yglobe_masterx()

def yglobe_masterx_essays():
    clear()
    print """---yGLOBE-----TheMasterX's Website--------------------------
Enter 'quit' to exit yGlobe

TheMasterX's Essays
------------------------------------------------

Unfortunately, I lost all my files to an apartment fire. I'll update this once I write 
another essay.

>Home

2042 - TheMasterX"""
    dinput()
    if temp == "home": yglobe_masterx()
    elif temp == "quit": yglobe()
    else: yglobe_masterx_essays()
    
def yglobe_masterx_xyzdos():
    clear()
    print """---yGLOBE-----TheMasterX's Website--------------------------
Enter 'quit' to exit yGlobe

XYZ-DOS - Text-based is the future
------------------------------------------------

  -News-----------------------------------------
  XYZ-DOS Upgrade package
	2/27/2042
    The XYZ-DOS upgrade package should be here by
	the time you read this. 
	                               Find Out >More
>Home


2042 - TheMasterX
"""
    if allvar['update1'] == 0:
        allvar['update1'] = 1
    dinput()
    if temp == "more": pass
    if temp == "home": yglobe_masterx()
    if temp == "quit": pass

def yglobe_masterx_about():
    clear()
    print """---yGLOBE-----TheMasterX's Website--------------------------
Enter 'quit' to exit yGlobe

About TheMasterX
------------------------------------------------

Hello. I am TheMasterX. I am the creator of the upcoming OS, XYZ-DOS. I am a big proprietary
enthusiast.

>Home

2042 - TheMasterX"""
    dinput()
    if temp == "home": yglobe_masterx()
    elif temp == "quit": yglobe()
    else: yglobe_masterx_about()

def yglobe_masterx_blogpost():
    clear()
    print """---yGLOBE-----TheMasterX's Website--------------------------
Enter 'quit' to exit yGlobe

Why proprietary software was the past, and now the future.
------------------------------------------------

dis page wuz deleted bye a 1337 H@XX04

>Home

2042 - TheMasterX"""
    dinput()
    if temp == "home": yglobe_masterx()
    elif temp == "quit": yglobe()
    else: yglobe_masterx_blogpost()
    
def yglobe_skyycommunity():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y    >Login   >Sign Up
--------------------------------------------------------"""
    if allvar['login'] == 1:
        print "Logged in as %s" % allvar['username']
        if allvar['newpm'] >= 1:
            print "You have %s new PMs." % allvar['newpm']
        print ">PM   >Logout   >User"
    print """
New Messages -
    >Now compatible with yGlobe.  NEW
    >OpenXYZ Getting closer.  NEW
Sticky Messages -
    Rules (LINK BROKEN)
    >XYZ-DOS Official Beta Download

									2042 Skyy
"""
    dinput()
    if temp == "log": yglobe_skyycommunity_login()
    elif temp == "sign": yglobe_skyycommunity_signup()
    elif temp == "pm": yglobe_skyycommunity_pm()
    elif temp == "now": yglobe_skyycommunity_ycompatible()
    elif temp == "openxyz": yglobe_skyycommunity_openxyz()
    elif temp == "xyzdos" or "xyz-dos" or "xyz": yglobe_skyycommunity_xyzdownload()
    elif temp == "logout":
        allvar['login'] = 0
        yglobe_skyycommunity()
    else: pass

def yglobe_skyycommunity_pm():
    clear()
    allvar['newpm'] = 0
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
-------------------------------------"""
    print "PMs:\n"
    if allvar['pm'] >= 1:
        print ">1 Subject: Hello? - Rainbow2028"
    if allvar['pm'] >= 2:
        print ">2 Subject: Re:Re:Hello? - Rainbow2028"
    dinput()
    if temp == "1": yglobe_skyycommunity_pm_hello()
    if temp == "2": yglobe_skyycommunity_pm_rehello()

def yglobe_skyycommunity_pm_hello():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
-------------------------------------
\nSubject: Hello?
From: Rainbow2028
--------------------------------------------------------------------------
Hi there. You may have seen my name in the ycontact list. As you may or may not know,
I am one of the beta testers. I know that you are one, too. Wanna be friends?
\n>Reply"""
    dinput()
    if temp == "reply":
        yglobe_skyycommunity_reply("pm")
        yglobe_skyycommunity_reply("newpm")
        yglobe_skyycommunity()
    else: pass

def yglobe_skyycommunity_pm_rehello():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
-------------------------------------
\nSubject: Re:Re:Hello?
From: Rainbow2028
--------------------------------------------------------------------------
Oh, my name? It's Emily, but you can just call me by my username, if that's okay with you.
What about yours? I suppose we can talk through ycontact.
\n>Reply"""
    dinput()
    if temp == "reply":
        allvar['contactnumberr'] = 1
        yglobe_skyycommunity()
    else: pass

def yglobe_skyycommunity_reply(id):
    allvar[id] += 1

def yglobe_skyycommunity_signup():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
--------------------------------------------------------
"""
    cinput("New Username? ")
    temp = allvar['username']
    cinput("New Password? ")
    temp = allvar['password']
    allvar['login'] = 1
    yglobe_skyycommunity()

def yglobe_skyycommunity_login():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
--------------------------------------------------------
"""
    cinput("Username? ")
    if temp == allvar['username']:
        cinput("Password? ")
        if temp == allvar['password']:
            allvar['login'] = 1
    yglobe_skyycommunity()

def yglobe_skyycommunity_ycompatible():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
--------------------------------------------------------

Topic: Now Compatible with yGlobe
------------------------------------------------------------------------------
                        :
By: Skyy                :  Just wanted to bring that update out there.
On 2/28/42              :  Thanks for all the support!
                        :
------------------------------------------------------------------------------

>Home                                                                2042 Skyy"""
    dinput()
    if temp == "home": yglobe_skyycommunity()
    else: pass

def yglobe_skyycommunity_openxyz():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
--------------------------------------------------------

Topic: OpenXYZ Getting closer
------------------------------------------------------------------------------
                        :
By: unseenhacker        :   Hey guys. Work is almost done on OpenXYZ. It
On 2/28/42              :   is an open-source XYZ-DOS clone. As you may know,
                        :   I am one of the XYZ-DOS beta testers. I am using 
                        :   my beta testing to research for OpenXYZ.
                        :   However I have released the unofficial package
                        :   nstaller. >Get it now
                        :
------------------------------------------------------------------------------
                        :
By: Skyy                :   I can't wait until it's released. I've
On 2/28/42              :   thought ever since you announced you were working
                        :   on it that it was going to be interesting.
                        :
                        :
------------------------------------------------------------------------------
                        :
By: H a p p y F a c e   :  This is great. Are the other 2 testers on here?
On 2/28/42              :
                        :
                        :

>Home                                                                2042 Skyy"""
    dinput()
    if temp == "get": install_hunter()
    elif temp == "home": yglobe_skyycommunity()
    else: pass

def yglobe_skyycommunity_xyzdownload():
    clear()
    print """---yGLOBE-----SkyyCommunity----------------------------------
Enter 'quit' to exit yGlobe

  S K Y Y  C O M M U N I T Y
--------------------------------------------------------

Topic - XYZ-DOS Official Beta
------------------------------------------------------------------------------
                        :
By: Skyy                :  I have gotten an ISO file of XYZ-DOS. The OS that
On 2/25/42              :  will bring back text-based OSes, and proprietory
                        :  software.
                        :
                        :  NOTE: Don't use the contact package.
                        :  Download link removed due to takedown.
                        :
------------------------------------------------------------------------------
                        :
By: unseenhacker        :  This seems interesting that you got a copy without
On 2/25/42              :  TheMasterX knowing. I can't tell if this works
                        :  because I am one of the beta testers, and XYZ-DOS
                        :  doesn't have a partition manager or a way to write
                        :  to a usb.
                        :
------------------------------------------------------------------------------
                        :
By: H a p p y F a c e   :  I'm going to try this.
On 2/26/42              :  EDIT 2/28/42: I tried this along with the tool
                        :  that unseenhacker made. Why is he 'bringing back
                        :  text-based'? It's dumb!
                        :
------------------------------------------------------------------------------

>Home"""
    dinput()
    if temp == "home": yglobe_skyycommunity()
    else: pass
    save()

def install_hunter():
    clear()
    print """This will install the 'hunter' package
on your computer. This package is not supported
by TheMasterX.

Do you still want to install?
>Yes
>No"""
    dinput()
    if temp == "yes" or "y":
        allvar['hunter'] = 1
        print "Installed"
        pause()
        yglobe_skyycommunity_openxyz()
    if temp == "no" or "n":
        yglobe_skyycommunity_openxyz()

def yglobe_ydownloadhost():
    clear()
    print """---yGLOBE-----YDownloadHost----------------------------------
Enter 'quit' to exit yGlobe

YDownloadHost is currently in construction. Thanks to all who volunteered in the first half of its
creation!"""
    dinput()
    if temp == "quit": yglobe()
    else: yglobe_ydownloadhost()

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()