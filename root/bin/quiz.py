import os, sys
import json
from etc import *

global quizgame

def quizgame():
    load()
    clear()
    print "Welcome to random quiz"
    print "\n1) Yay"
    print "2) GET ME OUT OF HERE"
    dinput()
    if temp == "1":
        clear()
        print "Question 1: What year is it?"
        print "\n1) 1776"
        print "2) 7076"
        print "3) 2042"
        print "4) 2015"
        dinput()
        if temp == "2042" or "3":
            print "CORRECT! (5 X-Points recieved.)"
            allvar['points'] += 5
            save()
            pause()
            clear()
            print "Question 2: What did the Source Crash of 2037 do on proprietary software?"
            print "\n1) Made it less popular"
            print "2) Made it more popular"
            print "3) Made it illegal"
            print "4) Made it completely destroy everything in sight"
            dinput()
            if temp == "1":
                print "CORRECT! (8 X-Points recieved.)"
                allvar['points'] += 8
                save()
                pause()
                clear()
                print "Question 3: Why did the open source community win the War of the Source Crash?"
                print "\n1) There was a sudden loss of proprietary supporters"
                print "2) The open-source community was quite developed and the biggest proprietary company went down."
                print "3) They didn't; closed source was always more used, even in 2042"
                print "4) NUCLEAR BOMBS!"
                dinput()
                if temp == "2":
                    print "CORRECT! (15 X-Points recieved.)"
                    allvar['points'] += 15
                    print "That's all folks!"
                    save()
                else:
                    print "wrong wrong wrong wrong."
                    save()
            else:
                print "wrong wrong wrong wrong."
                save()
        else:
            print "wrong wrong wrong wrong."
            save()

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()