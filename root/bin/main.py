#!/usr/bin/env python2

import os
import sys
import time
import datetime
import json
from quiz import *
from pkgmgr import *
from adventure import *
from adventure2 import *
from xedit import *
from ycontact import *
from yglobe import *
from hunter import *
from privatetalk import *
from xtoppoints import *
from yspy import *

def variableSetting():
    global allvar
    
    allvar = {
        'firstboot' : 1,
        'points' : 0,
        'quiz' : 0,
        'adventure' : 0,
        'adventure2' : 0,
        'edit' : 0,
        'name1' : 0,
        'xkernel2': 0,
        'contact' : 0,
        'xstorage' : 0,
        'xglobe' : 0,
        'name' : "user",
        'openxyz' : 0,
        'call1' : 0,
        'all2' : 0,
        'update1' : 0,
        'username' : "PixelHarmony2",
        'password' : "2secur4u",
        'login' : 0,
        'hunter' : 0,
        'points' : 0,
        'contactnumberx' : 0,
        'contactnumberu' : 0,
        'contactnumberr' : 0,
        'hpoints' : 20,
        'unseensh' : 0,
        'privatetalk' : 0,
        'x2ppoints' : 0,
        'yspy' : 0,
        'pm' : 0,
        'newpm' : 0
    }
    

def install():
    for i in xrange(10):
        clear()
        print "Well done, Ms. Harmony."
        print "We just needed beta testers."
        print "Thus we did a contest to see who would do it for us."
        print "You were one of the winners."
        time.sleep(0.3)
    clear()
    time.sleep(1)
    print """Congratulations for choosing XYZ-DOS.
We are working hard to bring you the best disk operating system.
It even fits on a Ultra Floppy Disk (4.4 GB of space)
\nFor best results, please run this fullscreen."""
    pause()
    clear()
    print "Now installing XYZ-DOS"
    time.sleep(1)
    print "Now copying system files..."
    time.sleep(1.5)
    print "cdrom/xyzsystem/ /xyzsystem/"
    time.sleep(0.5)
    print "cdrom/home/ /home/"
    time.sleep(0.5)
    print "cdrom/packages/ /packages/"
    time.sleep(0.5)
    print "cdrom/boot/ /boot/"
    time.sleep(0.5)
    print "cdrom/xyzsystem/xyzsh /xyzsystem/xyzsh"
    time.sleep(0.5)
    print "cdrom/xyzsystem/ufdrv /xyzsystem/ufdrv"
    time.sleep(0.5)
    print "cdrom/xyzsystem/usbdrv /xyzsystem/usbdrv"
    time.sleep(0.5)
    print "cdrom/xyzsystem/cddrv /xyzsystem/cddrv"
    time.sleep(0.5)
    print "cdrom/xyzsystem/pkgmgr /xyzsystem/pkgmgr"
    time.sleep(0.5)
    print "cdrom/boot/xkernel /boot/xkernel"
    time.sleep(0.5)
    print "cdrom/boot/xkernel-headers /boot/xkernel-headers"
    time.sleep(0.5)
    print "cdrom/boot/coreapi /boot/coreapi"
    time.sleep(0.5)
    print "cdrom/packages/pkginfo /packages/pkginfo"
    time.sleep(0.5)
    print "cdrom/packages/pkgdata/ /packages/pkgdata/"
    time.sleep(0.5)
    clear()
    print "Now finished. we are now loading XYZ-DOS..."
    time.sleep(4)
    print "Loading the X Kernel..."
    time.sleep(1.5)
    print "Loading the coreAPI..."
    time.sleep(1)
    print "Loading package info..."
    time.sleep(2)
    print "Loading XYZ-DOS..."
    time.sleep(5)
    clear()
    print "User \"TheMasterX\" Connecting..."
    time.sleep(10)
    print "TheMasterX: Hi there. You are now using my creation, XYZ-DOS"
    time.sleep(5)
    print "TheMasterX: I will send you packages that you will buy with earned points known as X-Points."
    time.sleep(5)
    print "TheMasterX: Packages are kinda like programs. They can also improve the OS itself."
    time.sleep(5)
    print "OK, I'll reboot the computer now. Cya."
    time.sleep(6)
    print "Remember to look at the help."
    allvar['firstboot'] = 0
    time.sleep(9)
    boot()
    
"""Just a normal startup function.
What is does is... I have no idea."""
def boot():
    clear()
    print "Loading the X Kernel..."
    time.sleep(1.5)
    print "Loading the coreAPI..."
    time.sleep(1)
    print "Loading package info..."
    time.sleep(2)
    print "Loading XYZ-DOS..."
    time.sleep(5)
    os.chdir('root/usr/home')
    now = datetime.datetime.now()
    if now.month == 10 and now.day == 21:
        print "Happy Birthday, Melody!"
    if now.day == 13 and now.weekday() == 4:
        print "Come one everyone!"
        print "Smile!"
        print "Just give me a grin!"
        print "From cheek to cheek!"
        print "Smile!"
        print "SmilE!"
        print "SmIlE"
        print "sMIlE"
    if now.year == 6969:
        print "I'm really feeling it!"
    clear()
    welcome()

def welcome():
    if allvar['unseensh'] == 1:
        print "Welcome to the UnseenSH"
        print "Now logged in as %s\n" % allvar['name']
        unseensh()
    else:
        print "Welcome to XYZ-DOS!"
        print "\nCopyright 2042 TheMasterX"
        print "Please type 'help' for more info and how to use goto links."
        xyzsh()
    criticalerror()
    
def xyzsh():
    while True:
        cinput(allvar['name']+"~$ ")
        if " " in temp:
            temp1 = temp.split(" ")
            temp2 = temp1[0]
            temp3 = temp1[1]
            if temp2 == "echo":
                print temp3
            if temp2 == "cd":
                os.chdir(temp3)
            if temp2 == "mkdir":
                os.mkdir(temp3)
            if temp2 == "rm":
                os.remove(temp3)
            if temp2 == "rmdir":
                os.rmdir(temp3)
            if temp2 == "cat":
                with open(temp3, 'r') as content_file:
                    content = content_file.read()
                print content
        if temp == "help":
            clear()
            print """This is help
\nTo select a goto link, type the first letter after the > in all lowercase, excluding puctuation and apostrophes (including 's)
\nType 'clear' to remove everything on the screen
Type 'echo' then a line of text to print that line of text
Type 'cd' then a directory to go into that directory
Type 'ls' to see all files in the current directory
Use 'mkdir' to create a directory
Use 'rm' to remove a file
Use 'rmdir' to remove a directory
Use 'cat' to view the contents of a file
Type 'halt' to save and turn the computer off
Type 'packages' to list all packages on the computer
Type 'pkgmgr' to launch the package manager
\nThere are no programs, they are called packages.
\nTo launch a package, just type in the name of it.
\nSome packages require some X-Points. They are earned by doing certain
tasks in a package."""
            pause()
        if temp == "pkginfo":
            print "Installed Packages:"
            print "xkernel - The X Kernel"
            print "coreapi - CoreAPI"
            print "xyzdrivers - Drivers"
            print "xyzsh - The XYZ Shell"
            print "pkginfo - List of packages"
            print "pkgmgr - Package Manager"
        if temp == "ls":
            os.system('dir' if os.name == 'nt' else 'ls')
        if temp == "clear":
            clear()
        if temp == "pkgmgr":
            save()
            pkgmgr()
            load()
        if temp == "save":
            os.chdir(sys.path[0]+'/../..')
            save()
            os.chdir('root/usr/home/')
        if temp == "halt":
            os.chdir(sys.path[0]+'/../..')
            save()
            quit()
        if temp == "quiz":
            if allvar['quiz'] == 1:
                save()
                quizgame()
                load()
        if temp == "adventure":
            if allvar['adventure'] == 1:
                save()
                adventuregame()
                load()
        if temp == "xedit":
            if allvar['edit'] == 1:
                save()
                xedit()
                load()
        if temp == "ycontact":
            if allvar['contact'] == 1:
                save()
                xcontact()
                load()
        if temp == "name":
            if allvar['name1'] == 1:
                changename()
        if temp == "adventure2":
            if allvar['adventure2'] == 1:
                save()
                adventure2game()
                load()
        if temp == "yglobe":
            if allvar['xglobe'] == 1:
                save()
                yglobe()
                load()
        if temp == "hunter":
            if allvar['hunter'] == 1:
                save()
                hunter()
                load()
        if temp == "allvar":
            print allvar

def unseensh():
    while True:
        cinput(allvar['name']+"~$ ")
        if " " in temp:
            temp1 = temp.split(" ")
            temp2 = temp1[0]
            temp3 = temp1[1]
            if temp2 == "echo":
                print temp3
            if temp2 == "cd":
                os.chdir(temp3)
            if temp2 == "mkdir":
                os.mkdir(temp3)
            if temp2 == "rm":
                os.remove(temp3)
            if temp2 == "rmdir":
                os.rmdir(temp3)
            if temp2 == "cat":
                with open(temp3, 'r') as content_file:
                    content = content_file.read()
                print content
            if temp2 == "x2ppoints":
                if allvar['x2ppoints'] == 1:
                    save()
                    xtoppoints(int(temp3))
                    load()
        if temp == "help":
            clear()
            print """These are the help lines for xyzsh. They have not been touched.
\nThis is help
\nTo select a goto link, type the first letter after the >, excluding puctuation and apostrophes (including 's)
\nType 'clear' to remove everything on the screen
Type 'echo' then a line of text to print that line of text
Type 'cd' then a directory to go into that directory
Type 'ls' to see all files in the current directory
Type 'halt' to save and turn the computer off
Type 'packages' to list all packages on the computer
Type 'pkgmgr' to launch the package manager
\nThere are no programs, they are called packages.
\nTo launch a package, just type in the name of it.
\nSome packages require some X-Points. They are earned by doing certain
tasks in a package."""
            pause()
        if temp == "pkginfo":
            print "Installed Packages:"
            print "xkernel - The X Kernel"
            print "coreapi - CoreAPI"
            print "xyzdrivers - Drivers"
            print "xyzsh - The XYZ Shell"
            print "pkginfo - List of packages"
            print "pkgmgr - Package Manager"
        if temp == "ls":
            os.system('dir' if os.name == 'nt' else 'ls')
        if temp == "clear":
            clear()
        if temp == "pkgmgr":
            save()
            pkgmgr()
            load()
        if temp == "save":
            os.chdir(sys.path[0]+'/../..')
            save()
            os.chdir('root/usr/home/')
        if temp == "halt":
            os.chdir(sys.path[0]+'/../..')
            save()
            quit()
        if temp == "quiz":
            if allvar['quiz'] == 1:
                save()
                quizgame()
                load()
        if temp == "adventure":
            if allvar['adventure'] == 1:
                save()
                adventuregame()
                load()
        if temp == "xedit":
            if allvar['edit'] == 1:
                save()
                xedit()
                load()
        if temp == "ycontact":
            if allvar['contact'] == 1:
                save()
                xcontact()
                load()
        if temp == "name":
            if allvar['name1'] == 1:
                changename()
        if temp == "adventure2":
            if allvar['adventure2'] == 1:
                save()
                adventure2game()
                load()
        if temp == "yglobe":
            if allvar['xglobe'] == 1:
                save()
                yglobe()
                load()
        if temp == "allvar":
            print allvar
        if temp == "hunter":
            if allvar['hunter'] == 1:
                save()
                hunter()
                load()
        if temp == "privatetalk":
            if allvar['privatetalk'] == 1:
                save()
                privatetalk()
                load()
        if temp == "x2ppoints":
            if allvar['x2ppoints'] == 1:
                print "Usage: x2ppoints (AMOUNT)"
        if temp == "yspy":
            if allvar['yspy'] == 1:
                save()
                yspy()
                load()

def changename():
    cinput("Set name: ")
    allvar['name'] = temp

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()

def panic():
    while True:
        clear()
        print """Dump Info:
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87ensnidvius&nvskhinNsiNIOU8fisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fsdjonsnus&nksjdnnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNSsindifv9n
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
nmuigprna9a8nw34UNn789hn4e8&NE$NN&NE87fw78WNn8&NEFN&ns87en8nN&ENnHNinnianwYHINIninfisniNIn908n&nifnn98ehfnNNS
NIn908n&nifnn98ehfnNNS&NE87fw78WNn8&NEFN&ns87eHNinnianwYsingiNSInsdginiunin&Dnisnd89hnnskden&nNjnkdsnkgnuiisN
[[]]]]]]]]]]]]]]]]]]][[[]][[[[[[]][[[[[[[[[[[]]]]]][[[[][][][[[]][[]][][[[[[[[[]]]]]]]][[[[[[]]]][[[]]][[[]]]
Panic at 37a abort
System Panic!"""
        pause()

load()

if allvar['firstboot'] == 1:
    variableSetting()
    install()
else:
    boot()
