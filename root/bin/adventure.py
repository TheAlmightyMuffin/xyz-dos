import os, sys, json

def adventuregame():
    clear()
    load()
    print "Adventure Game"
    print "\nYou arive at a river."
    print "What do you do?"
    print "\nGo to >village"
    print "Go across >river"
    dinput()
    if temp == "village":
        print "You arive in the village. There is a monster attacking."
        print "\n>Talk to villagers"
        print ">Attack Monster"
        dinput()
        if temp == "talk":
            print "While talking to the people in panic, the monster eats you."
        if temp == "attack":
            print "You slay the monster"
            print "YOU WIN!"
            print "You earned 10 X-Points"
            allvar['points'] += 10
        else:
            print "die"
    if temp == "river":
        print "You drown"
    else:
        print "die"

def pause():
    temp = raw_input()
def cinput(message):
    global temp
    temp = raw_input(message).lower()
def dinput():
    cinput(">> ")
def clear():
	os.system('cls' if os.name == 'nt' else 'clear')

def save(): # Ran at shutdown. There is a debug command for this in xyzsh.
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'wb') as fp:
	    json.dump(allvar, fp, indent=2)

def load():
    global allvar
    with open(sys.path[0]+'/../../root/usr/data/save.json', 'rb') as fp:
        allvar = json.load(fp)

def error():
    print "System Error"
    print "Press any key to recover"
    pause()

def criticalerror():
    while True:
        clear()
        print "CRITIAL SYSTEM ERROR"
        print "PLEASE REBOOT THE SYSTEM"
        pause()